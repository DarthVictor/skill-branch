'use strict'
const express = require('express')
const url = require('url')
// Express init
const app = express()
 
// Express run
const port = 3000 
function process(request, response) {
    response.header('Access-Control-Allow-Origin', '*')
    response.header('Access-Control-Allow-Credentials', true)
    response.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS')
    response.header('Access-Control-Allow-Headers', 'Content-Type')
    const url_parts = url.parse(request.url, true)
    const query = url_parts.query
    const queryFullname = query.fullname || ''
    const hasNonLetterSigns = queryFullname
                              .split('')
                              .filter((letter) => (
                                letter.toUpperCase() === letter.toLowerCase() && letter !== ' '  && letter !== '\''
                              ))
                              .length > 0
    if(hasNonLetterSigns){
      return response.send('Invalid fullname')
    }
    const capitalize = ([first,...rest]) => first.toUpperCase() + rest.join('').toLowerCase();
    const fullName = queryFullname.split(' ').filter((name) => name.length > 0).map(capitalize)

    if(fullName.length == 1){
      return response.send(fullName[0])
    }
    else if(fullName.length == 2){
      return response.send(fullName[1] + ' ' + fullName[0].slice(0,1) + '.')
    }
    else if(fullName.length == 3){
      return response.send(fullName[2] + ' ' + fullName[0].slice(0,1) + '. ' + fullName[1].slice(0,1) + '.')
    }
    else{
      return response.send('Invalid fullname')
    }
    
}
app.options('/', process);
app.get('/', process);
app.listen(port, (error) => {
    if (error) {
        console.error(error)
    }
    else {
        console.log('App running on port ' + port + '!')
    }
})