import express from 'express'
import cors from 'cors'
import Promise from 'es6-promise'
Promise.polyfill()
import fetch from 'isomorphic-fetch'
const petsUrl = 'https://gist.githubusercontent.com/isuvorov/55f38b82ce263836dadc0503845db4da/raw/pets.json'
fetch(petsUrl).then(async (res) => {
  runApp(await res.json())
})
.catch(err => {
  console.log('Something is wrong', err)
})


const runApp = (model)=> {

  const send404 = (res) => res.status(404).send('Not Found')
  const userById = (id) => model.users.filter(_ => _.id === Number(id))[0]
  const userByName = (username) => model.users.filter(_ => _.username === username)[0]
  const petsByUserId = (userId) => model.pets.filter(_ => _.userId === Number(userId))
  const petById = (id) => model.pets.filter(_ => _.id === Number(id))[0]
  const populatePet = (pet) => Object.assign({}, pet, {user: userById(pet.userId)}) 
  const populateUser = (user) => Object.assign({}, user, {pets: petsByUserId(user.id)}) 

  const app = express()
  app.use(cors())
  
  app.get('/', (req, res) => {
    return res.json(model)
  })

  const getUsersFromReq = (req) => (req.query.havePet ? 
                                  model.users.filter(u => model.pets.filter(p => p.userId === u.id && req.query.havePet === p.type).length) : 
                                  model.users)

  app.get('/users', (req, res) => {
    return res.json(getUsersFromReq(req))
  })

  app.get('/users/populate', (req, res) => {
    return res.json(getUsersFromReq(req).map(populateUser))
  })

  const getUserFromReq = (req) => (userById(req.params.userId) || userByName(req.params.userId))  

  app.get('/users/:userId', (req, res) => {
    const user = getUserFromReq(req)
    return user ? res.json(user) : send404(res)
  })

  app.get('/users/:userId/populate', (req, res) => {
    const user = getUserFromReq(req)
    return user ? res.json(populateUser(user)) : send404(res)
  })

  app.get('/users/:userId/pets', (req, res) => {
    const user = getUserFromReq(req)
    return user ? res.json(petsByUserId(user.id)) : send404(res)
  })

  const getPetsFromReq = (req) => (
     model.pets.filter(pet => {
        return (!req.query.type || pet.type === req.query.type) &&
              (!req.query.age_gt || pet.age > req.query.age_gt) &&
              (!req.query.age_lt || pet.age < req.query.age_lt)
        })
  )

  app.get('/pets', (req, res) => {
    const pets = getPetsFromReq(req)
    return res.json(pets)
  })

  app.get('/pets/populate', (req, res) => {
    const pets = getPetsFromReq(req)
    return res.json(pets.map(populatePet))
  })

  app.get('/pets/:petId', (req, res) => {
    const pet = petById(req.params.petId)
    return pet ? res.json(pet) : send404(res)
  })

  app.get('/pets/:petId/populate', (req, res) => {
    const pet = petById(req.params.petId)
    return pet ? res.json(populatePet(pet)) : send404(res)
  })

  app.use('*', (req, res, next) => {
    return send404(res)
  })

  app.listen(3000, () => {
    console.log('Your app listening on port 3000!')
  })

}