'use strict'
const express = require('express')
const url = require('url')
// Express init
const app = express()
 
// Express run
const port = 3000 
function process(request, response) {
    response.header('Access-Control-Allow-Origin', '*')
    response.header('Access-Control-Allow-Credentials', true)
    response.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS')
    response.header('Access-Control-Allow-Headers', 'Content-Type')
    const url_parts = url.parse(request.url, true)
    const query = url_parts.query
    const a = Number(query.a) || 0
    const b = Number(query.b) || 0
    console.log(a,b)
    response.send('' + (a + b)  )  
}
app.options('/', process);
app.get('/', process);
app.listen(port, (error) => {
    if (error) {
        console.error(error)
    }
    else {
        console.log('App running on port ' + port + '!')
    }
})