import express from 'express';
import cors from 'cors';

const app = express();
const send404 = (res) => res.status(404).send('Not Found')

app.use(cors());
app.get('/', (req, res) => {
  if(Number(req.query.i) >= 0){
      let values = {
        0: 1,
        1: 18,
        2: 243,
        3: 3240,
        4: 43254,
        5: 577368,
        6: 7706988,
        7: 102876480,
        8: 1373243544,
        9: 18330699168,
        10: 244686773808,
        11: 3266193870720,
        12: 43598688377184,
        13: 581975750199168,
        14: 7768485393179328,
        15: 103697388221736960,
        16: 1384201395738071424,
        17: 18476969736848122368,
        18: 246639261965462754048
      }
      if(!values[req.query.i]){
          return send404(res)
      }
      else{
          return res.send('' + values[req.query.i])
      }
  } 
  else {
      return send404(res)
  }
})

app.use('*', (req, res, next) => {
  return send404(res)
})

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
