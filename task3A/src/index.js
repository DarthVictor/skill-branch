import express from 'express';
import cors from 'cors';
import Promise from 'es6-promise';
Promise.polyfill()
import fetch from 'isomorphic-fetch';

const pcUrl = 'https://gist.githubusercontent.com/isuvorov/ce6b8d87983611482aac89f6d7bc0037/raw/pc.json';

fetch(pcUrl).then(async (res) => {
  const pc = await res.json();
  runApp(pc);
})
.catch(err => {
  console.log('Чтото пошло не так:', err);
});


const runApp = (pc)=> {

  const registerPcObject = (app, obj, prefix) =>{
      app.get(prefix, (req, res) => {
          res.json(obj);
      });
      if(typeof obj === 'object' && obj !== null /*&& !Array.isArray(obj) */){
          Object.keys(obj).forEach((objField) => {
            registerPcObject(app, obj[objField], prefix + objField + '/')
          });
      }
  }

  const getVolumes = (_hdds) => {
    const hdds = Array.isArray(_hdds) ? _hdds : [];
    let volumes = {};
    hdds.forEach((hdd) => {
      volumes[hdd.volume] = (Number((volumes[hdd.volume] || '0B').split('B')[0]) + hdd.size) + 'B';
    });
    return volumes
  }

  const app = express();
  app.use(cors());
  registerPcObject(app, pc, '/');
  const volumes = getVolumes(pc.hdd)
  app.get('/volumes', (req, res) => {
      res.json(volumes);
  });
  app.use(function (req, res, next) {
    res.status(404).send('Not Found')
  })
  app.listen(3000, () => {
    console.log('Your app listening on port 3000!');
  });

}