import express from 'express'
import cors from 'cors' 
import fetch from 'isomorphic-fetch'
import Promise from 'bluebird'
import _ from 'lodash'
const baseUrl = 'https://pokeapi.co/api/v2'
const csvUrl = 'https://raw.githubusercontent.com/PokeAPI/pokeapi/d0909e1cc31b4bc0ff8af669254bd3ff6308108d/data/v2/csv/pokemon.csv'
const __DEV__ = false 
const __CSV__ = false 

async function getPokemons(url, i = 0){
    console.log('getPokemons', url, i)

    const response = await fetch(url)
    const page = await response.json()
    const pokemons = page.results
    if(__DEV__ && i > 1) {
        return pokemons
    }
    if(page.next){
        const pokemons2 = await getPokemons(page.next, i + 1)
        return [
          ...pokemons,
          ...pokemons2
        ]
    }
    return pokemons
}
async function getPokemonsCSV(){
  const response = await fetch(csvUrl)
  const page = await response.text()
  const columnHeaders = ['id', 'identifier', 'species_id', 'height', 'weight', 'base_experience', 'order', 'is_default']
  const pokemons = page.split('\n').slice(1).map((row)=>{
    const cols = row.split(',').map(col => col.trim())
    return {
      id: +cols[0],
      name: cols[1],
      height: +cols[3],
      weight: +cols[4],
    }
  }).filter(pokemon => {
    return pokemon.id > 0  && pokemon.name && pokemon.height > 0 && pokemon.weight > 0
  })
  console.log(JSON.stringify(pokemons))
  return pokemons
}
async function getPokemon(url){
    const response = await fetch(url)
    return await response.json()
}

const pokemonFields = ['id', 'name', 'height', 'weight']

async function getData () {
  try{
    if(__CSV__) return await getPokemonsCSV()
    const pokemonsInfo = await getPokemons(`${baseUrl}/pokemon`)
    const smallPokemonsInfo = __DEV__ ? pokemonsInfo.slice(0, 30) : pokemonsInfo
    const pokemonsPromises = smallPokemonsInfo.map(info =>{
       return getPokemon(info.url)
    })
    const pokemonsFull = await Promise.all(pokemonsPromises)
    const pokemons = pokemonsFull.map(pokemon => _.pick(pokemon, pokemonFields))
    //const sortedPokemons = _.sortPokemons(pokemons, pokemon => pokemon.weight)
    return pokemons
    
  }
  catch(e){
    console.error(e)
    return res.json(e)
  }
    
}
getData().then((pokemons) => {

  const app = express();
  app.use(cors());
  function limitOffset(data, req){
    const limit = Number(req.query.limit) || 20
    const offset = Number(req.query.offset) || 0
    return data.slice(offset, limit + offset)
  }
  
  const sortPokemons = (pokemons, func) => {
    return pokemons.sort((a, b) =>{
        if(func(a) < func(b)) {
          return -1
        }
        else if(func(a) > func(b)) {
          return 1
        }
        else{
          if(a.name < b.name) return -1
          if(a.name > b.name) return 1
          return 0;
        }
    }).map(pokemon => {
      if(__DEV__) return pokemon
      else return pokemon.name
    })
  } 

  app.get('/',(req, res) => {
    return res.send(limitOffset(
      sortPokemons(pokemons, pokemon => pokemon.name),
      req))
  })

  app.get('/fat',(req, res) => {
    return res.send(limitOffset(
      sortPokemons(pokemons, pokemon => - (pokemon.weight / pokemon.height)),
      req))
  })

  app.get('/angular',(req, res) => {
    return res.send(limitOffset(
      sortPokemons(pokemons, pokemon => pokemon.weight / pokemon.height),
      req))
  })

  app.get('/heavy',(req, res) => {
    return res.send(limitOffset(
      sortPokemons(pokemons, pokemon => -pokemon.weight),
      req))
  })

  app.get('/light',(req, res) => {
    return res.send(limitOffset(
      sortPokemons(pokemons, pokemon => pokemon.weight),
      req))
  })

  app.get('/huge',(req, res) => {
    return res.send(limitOffset(
      sortPokemons(pokemons, pokemon => -1 * pokemon.height),
      req))
  })

  app.get('/micro',(req, res) => {
    return res.send(limitOffset(
      sortPokemons(pokemons, pokemon => pokemon.height),
      req))
  })
   
  app.listen(3000, () => {
    console.log('Your app listening on port 3000!');
  });

})

