import express from 'express';
import cors from 'cors';
import Color from 'color-js'

const app = express();
app.use(cors());
app.get('/', (req, res) => {
    const queryColor = (req.query.color || '').replace(/%20/g, ' ').trim()
    const color = (queryColor.charAt(0) !== '#' && queryColor.slice(0,3) !== 'hsl' && queryColor.slice(0,3) !== 'rgb' ? '#' : '') + queryColor     
    const cssColor = (new Color(color)).toCSS().toLowerCase()
    if(cssColor.charAt(0) === '#' && Color.isValid(color) && additionalFilters(color)){
      return res.send(cssColor)
    }
    else {
      return res.status(404).send('Invalid color')
    }
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});

function additionalFilters (color){
  let css_integer = '(?:\\+|-)?\\d+';
  let css_float = '(?:\\+|-)?\\d*\\.\\d+';
  let css_number = '(?:' + css_integer + ')|(?:' + css_float + ')';
  css_integer = '(' + css_integer + ')';
  css_float = '(' + css_float + ')';
  css_number = '(' + css_number + ')';
  let css_percentage = css_number + '%';
  let css_whitespace = '\\s*?';
  let rgb_rgba_integer_regex = RegExp([
      '^rgb(a?)\\(', css_integer, ',', css_integer, ',', css_integer, '(,(', css_number, '))?\\)$'
    ].join(css_whitespace) );
  let hsl_hsla_regex = new RegExp([
      '^hsl(a?)\\(', css_number, ',', css_percentage, ',', css_percentage, '(,(', css_number, '))?\\)$'
    ].join(css_whitespace) );
  let matchResultRgba = color.match(rgb_rgba_integer_regex)
  let matchResultHsla = color.match(hsl_hsla_regex)
  if(matchResultRgba){
    return Number(matchResultRgba[2]) >= 0 && Number(matchResultRgba[2]) < 256 && 
          Number(matchResultRgba[3]) >= 0 && Number(matchResultRgba[3]) < 256 && 
          Number(matchResultRgba[4]) >= 0 && Number(matchResultRgba[4]) < 256 &&
          (!matchResultRgba[7] || Number(matchResultRgba[7]) <= 1 && Number(matchResultRgba[7]) >= 0)
  }
  else if(matchResultHsla){
    return Number(matchResultHsla[2]) >= 0 && Number(matchResultHsla[2]) <= 360 && 
          Number(matchResultHsla[3]) >= 0 && Number(matchResultHsla[3]) <= 100 && 
          Number(matchResultHsla[4]) >= 0 && Number(matchResultHsla[4]) <= 100 &&
          (!matchResultHsla[7] || Number(matchResultHsla[7]) <= 1 && Number(matchResultHsla[7]) >= 0)
  }
  else {
    return true
  }
}
