import express from 'express';
import cors from 'cors';

const app = express();
app.use(cors());
app.get('/', (req, res) => {
  const url = req.query.username

  const path = url
        .replace(/^(https?\:)/, '')
        .replace(/^\/\//, '')
  const pathArray = path.split('/')
  const username = pathArray.length > 1 ? pathArray[1] : pathArray[0]
  res.send('@' + username.replace(/^@/, ''));
});

app.listen(3000, () => {
  console.log('Your app listening on port 3000!');
});
